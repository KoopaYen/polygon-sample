package com.evertrust.drawonmap.app.map;

import android.content.Context;
import android.view.MotionEvent;
import android.widget.FrameLayout;

public class MapWrapperLayout extends FrameLayout {
    private OnDragListener mOnDragListener;

    public interface OnDragListener {
        public void onDrag(MotionEvent motionEvent);
    }

    public MapWrapperLayout(Context context) {
        super(context);
    }

    public void setOnDragListener(OnDragListener mOnDragListener) {
        this.mOnDragListener = mOnDragListener;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (mOnDragListener != null) {
            mOnDragListener.onDrag(ev);
        }
        return super.dispatchTouchEvent(ev);
    }
}
