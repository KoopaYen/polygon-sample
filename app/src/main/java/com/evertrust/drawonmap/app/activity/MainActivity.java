package com.evertrust.drawonmap.app.activity;

import android.graphics.Color;
import android.graphics.Point;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

import com.evertrust.drawonmap.app.R;
import com.evertrust.drawonmap.app.map.MapFrameLayout;
import com.evertrust.drawonmap.app.polygon.Polygon;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolygonOptions;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class MainActivity extends ActionBarActivity implements View.OnClickListener, View.OnTouchListener {
    private static final String TAG = "MainActivity";
    private GoogleMap mGoogleMap;
    private Polygon mCustomizePolygon;
    private SupportMapFragment mMapFragment;

    private double mLatitude;
    private double mLongitude;
    private boolean mIsMapMoveable;
    private ArrayList<LatLng> mCoordinateList;
    private ArrayList<LatLng> mSamplePinList;

    @InjectView(R.id.frame_map) MapFrameLayout mMapFrame;
    @InjectView(R.id.btn_draw_state) Button mDrawStateBtn;
    @OnClick(R.id.btn_draw_state) void drawBtnOnClick() {
        mIsMapMoveable = !mIsMapMoveable;
        mDrawStateBtn.setText(mIsMapMoveable ? R.string.stop_draw : R.string.app_name);
        // Clear Polygon
        if (mIsMapMoveable) {
            mCoordinateList.clear();
            mGoogleMap.clear();
            addSampleMarks();
        }
        // Clear Paint on MapFrame.
        mMapFrame.setIsClearPaint(!mIsMapMoveable);
        mMapFrame.invalidate();
        mMapFrame.setVisibility(mIsMapMoveable ? View.VISIBLE : View.GONE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(MainActivity.this);

        mMapFragment = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map));

        mCoordinateList = new ArrayList<LatLng>();
        mIsMapMoveable = false;

        mMapFrame.setOnTouchListener(MainActivity.this);
        mMapFrame.setWillNotDraw(false);
    }

    @Override
    protected void onStart() {
        super.onStart();
        initMap();
        addSampleMarks();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View view) {}

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        float x = event.getX();
        float y = event.getY();

        mMapFrame.setCoordinateOnScreen(x, y);
        mMapFrame.invalidate();

        int x_co = Math.round(x);
        int y_co = Math.round(y);

        Point x_y_points = new Point(x_co, y_co);
        LatLng latLng = mGoogleMap.getProjection().fromScreenLocation(x_y_points);
        mLatitude = latLng.latitude;
        mLongitude = latLng.longitude;

        int eventaction = event.getAction();
        switch (eventaction) {
            case MotionEvent.ACTION_DOWN:
                // finger touches the screen
                mCoordinateList.add(new LatLng(mLatitude, mLongitude));
                break;
            case MotionEvent.ACTION_MOVE:
                // finger moves on the screen
                mCoordinateList.add(new LatLng(mLatitude, mLongitude));
                break;
            case MotionEvent.ACTION_UP:
                // finger leaves the screen
                drawOnMap();
                drawBtnOnClick();
                mMapFrame.setVisibility(View.GONE);
                showMarkerInPolygon();
                break;
        }

        return mIsMapMoveable;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initMap() {
        if (mMapFragment != null) {
            mGoogleMap = mMapFragment.getMap();
            if (mGoogleMap != null) {
                UiSettings uiSettings = mGoogleMap.getUiSettings();
                uiSettings.setAllGesturesEnabled(false);
                uiSettings.setScrollGesturesEnabled(true);
                uiSettings.setZoomGesturesEnabled(true);
                mGoogleMap.setMyLocationEnabled(true);
                mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(25.030284, 121.549201), 13));
            }
        }
    }

    private void drawOnMap() {
        PolygonOptions options = new PolygonOptions();
        options.addAll(mCoordinateList);
        options.strokeColor(Color.BLUE);
        options.strokeWidth(7);
        mGoogleMap.addPolygon(options);
    }

    private void showMarkerInPolygon() {
        ArrayList<com.evertrust.drawonmap.app.polygon.Point> pointArrayList = new ArrayList<com.evertrust.drawonmap.app.polygon.Point>();
        ArrayList<LatLng> newPoints = new ArrayList<LatLng>();
        com.evertrust.drawonmap.app.polygon.Polygon.Builder polygonBuild = new com.evertrust.drawonmap.app.polygon.Polygon.Builder();

        for (LatLng latLng : mCoordinateList) {
            com.evertrust.drawonmap.app.polygon.Point point = new com.evertrust.drawonmap.app.polygon.Point((float) latLng.latitude, (float) latLng.longitude);
            polygonBuild.addVertex(point);
        }
        mCustomizePolygon = polygonBuild.build();

        for (LatLng latLng : mSamplePinList) {
            pointArrayList.add(new com.evertrust.drawonmap.app.polygon.Point((float) latLng.latitude, (float) latLng.longitude));
        }
        for (com.evertrust.drawonmap.app.polygon.Point point : pointArrayList) {
            if (mCustomizePolygon.contains(point)) {
                newPoints.add(new LatLng(point.x, point.y));
            }
        }

        mSamplePinList.clear();
        mGoogleMap.clear();
        drawOnMap();
        for (LatLng latLng : newPoints) {
            mGoogleMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_launcher)));
        }
    }

    private void addSampleMarks() {
        mSamplePinList = new ArrayList<LatLng>();
        mSamplePinList.add(new LatLng(25.021096, 121.548784));
        mSamplePinList.add(new LatLng(25.039360, 121.548383));
        mSamplePinList.add(new LatLng(25.037070, 121.521074));
        mSamplePinList.add(new LatLng(25.081280, 121.586680));
        mSamplePinList.add(new LatLng(25.075802, 121.575545));
        mSamplePinList.add(new LatLng(25.069702, 121.574981));

        for (LatLng latLng : mSamplePinList) {
            mGoogleMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_launcher)));
        }
    }
}
