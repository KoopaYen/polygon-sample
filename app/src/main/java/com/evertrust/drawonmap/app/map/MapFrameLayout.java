package com.evertrust.drawonmap.app.map;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import java.util.ArrayList;

public class MapFrameLayout extends FrameLayout {
    private boolean mIsClearPaint;
    private Paint mPaint;
    private ArrayList<CoordinateObject> mCoordinateList;

    public MapFrameLayout(Context context) {
        super(context);
        initialPaint();
        mCoordinateList = new ArrayList<CoordinateObject>();
    }

    public MapFrameLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        initialPaint();
        mCoordinateList = new ArrayList<CoordinateObject>();
    }

    public void initialPaint() {
        mPaint = new Paint();
        mPaint.setColor(Color.BLUE);
        mPaint.setStrokeWidth(7);
    }

    public void setIsClearPaint(boolean isClearPaint) {
        mIsClearPaint = isClearPaint;
        if (mIsClearPaint) {
            mCoordinateList.clear();
        }
    }

    public void setCoordinateOnScreen(float x, float y) {
        mCoordinateList.add(new CoordinateObject(x, y));
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (mIsClearPaint) {
            canvas.drawColor(Color.TRANSPARENT);
        } else {
            for (CoordinateObject object : mCoordinateList) {
                canvas.drawPoint(object.getX(), object.getY(), mPaint);
            }
        }
    }

    class CoordinateObject {
        private float x;
        private float y;

        public CoordinateObject(float x, float y) {
            this.x = x;
            this.y = y;
        }

        public void setX(float x) {
            this.x = x;
        }

        public float getX() {
            return this.x;
        }

        public void setY(float y) {
            this.y = y;
        }

        public float getY() {
            return this.y;
        }
    }
}
